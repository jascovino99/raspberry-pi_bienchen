from influxdb_client import Point, WritePrecision
from influxdb_init_test import client, write_api, bucket, org
from random import randint
from time import sleep

def run_bienen_zähler():
    while True:
        # Simulate random bee count and temperature
        bee_count = randint(0, 100)
        temperature = randint(20, 35)
        
        # Create InfluxDB points with simulated data
        bee_point = Point("bee_data").field("count", bee_count)
        
        # Write points to InfluxDB
        write_api.write(bucket=bucket, org=org, record=bee_point)
        
        # Adjust the delay according to the frequency you want to simulate data
        sleep(5)  # Simulate data every 5 seconds

if __name__ == "__main__":
    run_bienen_zähler()
