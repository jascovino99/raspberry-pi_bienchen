from influxdb_client import Point, WritePrecision
from influxdb_init_test import client, write_api, bucket, org
from random import uniform
from time import sleep

def run_waage():
    while True:
        # Simulate random weight
        weight = round(uniform(0, 5), 2)  # Simulate weight between 0 and 5 kg with 2 decimal places
        
        # Create InfluxDB point with simulated data
        weight_point = Point("weight_data").field("weight", weight)
        
        # Write point to InfluxDB
        write_api.write(bucket=bucket, org=org, record=weight_point)
        
        # Adjust the delay according to the frequency you want to simulate data
        sleep(2)  # Simulate data every 2 seconds

if __name__ == "__main__":
    run_waage()
