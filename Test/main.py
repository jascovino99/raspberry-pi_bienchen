import multiprocessing
from bienen_zähler_test import run_bienen_zähler
from schallsensor_test import run_schallsensor
from waage_test import run_waage

if __name__ == "__main__":
    # Create processes for each script
    process1 = multiprocessing.Process(target=run_bienen_zähler)
    process2 = multiprocessing.Process(target=run_schallsensor)
    process3 = multiprocessing.Process(target=run_waage)

    # Start the processes
    process1.start()
    process2.start()
    process3.start()

    # Wait for the processes to finish
    process1.join()
    process2.join()
    process3.join()