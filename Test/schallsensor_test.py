from influxdb_client import Point, WritePrecision
from influxdb_init_test import client, write_api, bucket, org
from random import randint
from time import sleep

def run_schallsensor():
    while True:
        # Simulate random sound level
        sound_level = randint(50, 100)
        
        # Create InfluxDB point with simulated data
        sound_point = Point("sound_data").field("level", sound_level)
        
        # Write point to InfluxDB
        write_api.write(bucket=bucket, org=org, record=sound_point)
        
        # Set a very small sleep time (adjust as needed)
        sleep(0.01)  # Simulate data as fast as possible

if __name__ == "__main__":
    run_schallsensor()
