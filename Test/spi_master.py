import spidev
import time

# Initialize the SPI bus
spi = spidev.SpiDev()
spi.open(0, 0)  # Open SPI bus 0, device (CS) 0
spi.max_speed_hz = 50000

def spi_transfer(data):
    resp = spi.xfer2(data)
    return resp

try:
    while True:
        send_data = [0x42]  # Example data to send (0x42 is arbitrary)
        print(f"Sending: {send_data[0]}")
        response = spi_transfer(send_data)
        print(f"Received: {response[0]}")
        time.sleep(1)
except KeyboardInterrupt:
    spi.close()
