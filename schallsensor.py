from influxdb_client import Point, WritePrecision
from influxdb_init import write_api
from influxdb_init import client, write_api, bucket, org
from time import sleep
import busio
import board
import adafruit_mcp342x

delay = 1

def run_schallsensor():
    # TODO: 
    i2c = busio.I2C(board.SCL, board.SDA) # initialize I2C bus
    mcp3421 = adafruit_mcp342x.MCP3421(i2c)

    while True:
        # Read from MCP3421
        
        value = mcp3421.value

      
        # Write data to InfluxDB
        point = Point("sensor_data").field("Schallsensor", value)
        write_api.write(bucket=bucket, org=org, record=point)

        sleep(delay)


if __name__ == "__main__":
    run_schallsensor()