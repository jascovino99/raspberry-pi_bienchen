from influxdb_client import Point, WritePrecision
from influxdb_init import write_api, bucket, org  # Assuming client and write_api are imported in influxdb_init
from time import sleep
import random

# Define the desired frequency in Hz
frequency = 240
delay = 1 / frequency

def generate_random_value():
    # Generate a random value within a desired range
    return random.randint(0, 100)

def run_schallsensor():
    counter=0
    while True:
        # Generate a random value
        value = generate_random_value()

        counter+= counter
      
        
            
        sleep(delay)

if __name__ == "__main__":
    run_schallsensor()