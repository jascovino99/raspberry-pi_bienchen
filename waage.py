from influxdb_client import Point, WritePrecision
from influxdb_init import write_api
from influxdb_init import client, write_api, bucket, org

def run_waage():
    # TODO: Implementiere die Logik zum Lesen von echten Daten vom Gewichtssensor.
    #       Erfasse Gewichts vom Sensor.
    #       Erstelle InfluxDB-Punkte mit den echten Daten.
    #       Schreibe die Punkte in die InfluxDB mithilfe des bereitgestellten write_api.
    pass  # Diese Zeile entfernen, wenn die Logik implementiert ist.

if __name__ == "__main__":
    run_waage()
